package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.CursoRepository;
import domain.Curso;


@Repository
public class JpaCursoRepository extends JpaBaseRepository<Curso, Long> implements CursoRepository {
	@Override
	public Curso BuscarCodigo (String codigo) {
		
		String jpaQuery = "SELECT a FROM Curso a WHERE a.codigo = :codigo";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("codigo", codigo);
		return getFirstResult(query);
	}

	@Override
	public Collection<Curso> BuscarAlumnoId(Long alumnoId) {
		String jpaQuery = "SELECT a FROM Curso a JOIN a.owners p WHERE p.id = :alumnoId";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("alumnoId", alumnoId);
		return query.getResultList();
	}

}
