package repository.jpa;

import javax.persistence.TypedQuery;

import repository.AlumnoRepository;
import domain.Alumno;

public class JpaAlumnoRepository extends JpaBaseRepository <Alumno, Long> implements AlumnoRepository {

	@Override
	public Alumno BuscarAlumApP (String apellidoPaterno){
		
		String jpaQuery = "SELECT a FROM Alumno a WHERE a.apellidoPaterno = :apellidoPaterno";
		TypedQuery<Alumno> query = entityManager.createQuery(jpaQuery, Alumno.class);
		query.setParameter("apellidoPaterno", apellidoPaterno);
		return getFirstResult(query);
	}
	

	
}
