package repository;

import java.util.Collection;

import domain.Curso;

public interface CursoRepository extends  BaseRepository<Curso, Long>{

	Curso BuscarCodigo(String codigo);
	
	Collection<Curso> BuscarAlumnoId(Long alumnoId);
	
}
