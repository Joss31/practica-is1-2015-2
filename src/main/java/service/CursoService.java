package service;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.CursoRepository;
import repository.AlumnoRepository;
import domain.Curso;
import domain.Alumno;

@Service
public class CursoService {
	@Autowired
	CursoRepository cursoRepository;
	
	@Autowired
	AlumnoRepository alumnoRepository;

	@Transactional
	public void save(Curso curso) {
		cursoRepository.persist(curso);
	}

	@Transactional
	public void createCurso (Collection<Long> ownerIds, Curso curso) {
		if (!ownerIds.isEmpty()) {
			Collection<Alumno> owner = alumnoRepository.findByIds(ownerIds);

			cursoRepository.persist(curso);
		}
	}

	public Collection<Curso> getCursoporALumnoId(Long alumnoId) {
		return cursoRepository.BuscarAlumnoId(alumnoId);
	}


}
